# DaVinci Resolve - Fusion expressions

autor: TomaszFlisPL

## O projekcie

W tym projekcie znajdziesz przykłady kodów źródłowych do różnych skryptów dodawanych jako **Expression** w programie DaVinci Resolve - Fusion dzięki którym znacznie szybciej stworzysz pożądaną animację. Skrypty będą pogrupowane tematycznie w poszczególnych folderach, w których znajdziesz: plik(lub kilka plików) .txt z gotowym skryptem do wklejenia oraz plik Readme.md z opisem jak ten kod działa i jak go zmodyfikować do własnych potrzeb  

## Spis treści:
- [Countdown Timer - minutnik odliczający czas od zadanej wartości do 0](Countdown_Timer)
- [Clients Counter - licznik zwiększający wartość bardzo szybko od 0 do zadanej wartości. _Np. liczba klientów, liczba sprzedanych egzemplarzy itp._](Clients_counter)



***


