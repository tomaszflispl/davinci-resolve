# DaVinci Resolve - Fusion expressions

* autor: TomaszFlisPL


## Our_Clients_Counter - Licznik od 0 do zadanej wartości

Prosty sposób jak stworzyć animację szybkiego licznika, który w kilka sekund wzrośnie od 0 do zadanej wartości: 

przykład zastosowania: "liczba naszych klientów"

[Tutorial na YT - w trakcie tworzenia](https://YouTube.com/TomaszFlisPL/) 


### Instrukcja wykonania

Dodaj nowy node **Text+**, zaznacz go i w panelu **Inspector** kliknij prawym przyciskiem myszy na pole do wpisywania tekstu -> wybierz **Expression** i w polu, które pojawi się poniżej wklej kod generujący tekst licznika do wyświetlenia:

```
:counter=time*380; 
return iif(counter<20000, Text(string.format("%01.f",counter)), 20000);
```

Tekst do wyświetlenia będzie generowany automatycznie na podstawie numeru aktualnej klatki animacji.


### Co zmieniamy:
- wartość liczbowa w zmiennej `counter`: `time` oznacza numer bieżącej klatki kompozycji fusion, który mnożymy przez jakąś wartość żeby licznik rósł szybciej (tutaj: każda kolejna klatka to przyrost licznika o 380 - można dobrać indywidualną wartość w zależności od wartości do jakiej ma zliczać i pożądanej szybkości przyrostu, albo wymyślić inny sposób przekształcenia liczby klatek np. żeby licznik nie rósł liniowo tylko w jakiś inny sposób)
- w instrukcji warunkowej `iif` ustawiamy jaka ma być maksymalna wartość licznika na której zatrzymuje się liczenie (tutaj: 20000)


### Objaśnienie działania skryptu

- ```:counter=time*380;```

Obliczenie wartości jaka ma się pojawić na ekranie na podstawie numeru klatki animacji (tutaj: przyrost liczby co każdą klatkę o 380)

- ```return iif(counter<20000, Text(string.format("%01.f",counter)), 20000);```

Wyświetlenie odpowiedniego tekstu:

`iif(...)` - instrukcja warunkowa: jeżeli `counter` ma wartość mniejszą niż 20000 to wyświetl wartość tej zmiennej jako tekst, w przeciwnym wypadku wyświetl wartość "20000"


---







