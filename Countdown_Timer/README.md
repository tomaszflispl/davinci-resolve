# DaVinci Resolve - Fusion expressions

* autor: TomaszFlisPL


## CountdownTimer - Zegar odliczający czas od zadanej wartości do 0

Prosty sposób jak stworzyć minutnik odliczający zadany czas.

[Tutorial na YT - w trakcie tworzenia](https://YouTube.com/TomaszFlisPL/) 


### Instrukcja wykonania

Dodaj nowy node **Text+**, następnie zaznacz go i w panelu **Inspektor** kliknij prawym przyciskiem myszy na pole do wpisywania tekstu -> wybierz **Expression** i w polu które pojawi się poniżej wklej kod generujący tekst licznika do wyświetlenia:

```
:framerate=25; startseconds=3000; startcounter=startseconds*framerate; 
minutes=floor((startcounter-time)/framerate/60)%60; 
seconds=floor((startcounter-time)/framerate)%60; 
return Text(string.format("%02.f",minutes)..":"..string.format("%02.f",seconds));
```
Tekst do wyświetlenia będzie generowany automatycznie na podstawie numeru aktualnej klatki animacji.

### Co zmieniamy:
- `framerate` = liczba klatek na sekundę dla kompozycji fusion (żeby prawidłowo wyliczał czas trwania jednej sekundy)
- `startseconds` = liczba sekund do odliczenia (tutaj: 50 min = 50 * 60 s = 3000 s)

### Objaśnienie działania skryptu

- ```:framerate=25; startseconds=3000; startcounter=startseconds*framerate; ```

Zmienne potrzebne do dalszych obliczeń

- ```minutes=floor((startcounter-time)/framerate/60)%60; ```

Obliczenie liczby minut: 

`startcounter-time` - ile klatek pozostało do końca odliczania

`/framerate` - przeliczenie liczby klatek na liczbę sekund

`/60` - zamiana sekund na minuty

`floor(...)` - zaokrąglenie wyniku dzielenia w dół do liczby całkowitej

`%60` - reszta z dzielenia przed 60 - gdyby czas trwania był ustawiony na ponad godzinę, np. 71 min to wyświetlona zostanie końcówka ponad godzinę, tj. "11" - możemy analogicznie dodać kolejną zmienną "hours" i rozszerzyć licznik na dłuższy czas, ewentualnie usunąć dzielenie modulo i dopuścić wyświetlanie minut powyżej 59, bez wyciągania z nich liczby pełnych godzin

- ```seconds=floor((startcounter-time)/framerate)%60;```

Obliczenie liczby sekund - analogicznie jak dla minut powyżej.

- ```return Text(string.format("%02.f",minutes)..":"..string.format("%02.f",seconds));```

Wyświetlenie odpowiedniego tekstu:

`Text(...)` - funkcja odpowiadająca za wpisanie tekstu w node **Text+**

`string.format("%02.f", zmienna)` - konwersja zmiennej liczbowej na tekst składający się z 2 znaków (tj. jeżeli np. liczba sekunkd będzie mniejsza niż 10 to zostanie dopisane zero przed cyfrą, np. "05")

---


